
let directory = [
	{
		"name" : "Rendon",
		"email" : "Rendon@gmail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@email.com"
	},
	{
		"name" : "CongTV",
		"email" : "CongTV@email.com"
	},
	{
		"name" : "PINGRIS",
		"email" : "PINGRIS@email.com"
	}
];

let http = require("http");
const server = http.createServer(function(request, response){
    if(request.url == "/users" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.write(JSON.stringify(directory));
        response.end();
    }else if(request.url == "/users" && request.method == "POST"){

        let requestBody = '';

		//A stream is sequence of data
		// data is recieved from the client and is processed in the data stream
		// the information provided from the request object enters a sequence called data the code below will be triggered
		//data step this reads the data stream and processes it as the request.body

        request.on('data', function(data){
            requestBody += data
        })

		request.on('end', function(){
			// Check if at this point the requestBody is of data type STRING
			// We need this to be of data type JSON to access its property.

			console.log(typeof requestBody);

			// JSON.parse() - Takes a JSON string and transform it into javascript objects.
			
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type' : 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})
		
    }
}).listen(4000);

console.log("server running at port 4000");
let courses = [
	{
		"name" : "Programming",
		"code" : "101"
	},
	{
		"name" : "Science",
		"code" : "102"
	},
	{
		"name" : "Math",
		"code" : "103"
	}
];

let http = require("http");
const server = http.createServer(function(request, response){

    
    if(request.url == "/profile" && request.method == "GET"){
        response.writeHead(200, {'Content-type' : 'text/plain'});
        response.end('Welcome to your profile');
    }else if(request.url == "/courses" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.write("here are the courses available");
        response.write(JSON.stringify(courses));
        response.end();
    }else if(request.url == "/addcourse" && request.method == "POST"){
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.write("Add a course");
        response.end();
    }else if(request.url == "/updatecourse" && request.method == "PUT"){
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.write("Update a course");
        response.end();
    }else if(request.url == "/archivecourse" && request.method == "DELETE"){
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.write("Archive a course");
        response.end();
    }else{
        response.writeHead(200, {'Content-type' : 'text/plain'});
        response.end('Welcome to booking system');
    }
}).listen(4000);

console.log("server running at port 4000");